package com.ruoyi.pomanage.mapper;

import java.util.List;
import com.ruoyi.pomanage.domain.PomanageSapProject;

/**
 * SAP开发项目Mapper接口
 * 
 * @author ruoyi
 * @date 2023-07-12
 */
public interface PomanageSapProjectMapper 
{
    /**
     * 查询SAP开发项目
     * 
     * @param projectId SAP开发项目主键
     * @return SAP开发项目
     */
    public PomanageSapProject selectPomanageSapProjectByProjectId(Long projectId);

    /**
     * 查询SAP开发项目列表
     * 
     * @param pomanageSapProject SAP开发项目
     * @return SAP开发项目集合
     */
    public List<PomanageSapProject> selectPomanageSapProjectList(PomanageSapProject pomanageSapProject);

    /**
     * 新增SAP开发项目
     * 
     * @param pomanageSapProject SAP开发项目
     * @return 结果
     */
    public int insertPomanageSapProject(PomanageSapProject pomanageSapProject);

    /**
     * 修改SAP开发项目
     * 
     * @param pomanageSapProject SAP开发项目
     * @return 结果
     */
    public int updatePomanageSapProject(PomanageSapProject pomanageSapProject);

    /**
     * 删除SAP开发项目
     * 
     * @param projectId SAP开发项目主键
     * @return 结果
     */
    public int deletePomanageSapProjectByProjectId(Long projectId);

    /**
     * 批量删除SAP开发项目
     * 
     * @param projectIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePomanageSapProjectByProjectIds(Long[] projectIds);
}
