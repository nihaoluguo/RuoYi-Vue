package com.ruoyi.pomanage.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.pomanage.mapper.PomanageSapProjectMapper;
import com.ruoyi.pomanage.domain.PomanageSapProject;
import com.ruoyi.pomanage.service.IPomanageSapProjectService;

/**
 * SAP开发项目Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-07-12
 */
@Service
public class PomanageSapProjectServiceImpl implements IPomanageSapProjectService 
{
    @Autowired
    private PomanageSapProjectMapper pomanageSapProjectMapper;

    /**
     * 查询SAP开发项目
     * 
     * @param projectId SAP开发项目主键
     * @return SAP开发项目
     */
    @Override
    public PomanageSapProject selectPomanageSapProjectByProjectId(Long projectId)
    {
        return pomanageSapProjectMapper.selectPomanageSapProjectByProjectId(projectId);
    }

    /**
     * 查询SAP开发项目列表
     * 
     * @param pomanageSapProject SAP开发项目
     * @return SAP开发项目
     */
    @Override
    public List<PomanageSapProject> selectPomanageSapProjectList(PomanageSapProject pomanageSapProject)
    {
        return pomanageSapProjectMapper.selectPomanageSapProjectList(pomanageSapProject);
    }

    /**
     * 新增SAP开发项目
     * 
     * @param pomanageSapProject SAP开发项目
     * @return 结果
     */
    @Override
    public int insertPomanageSapProject(PomanageSapProject pomanageSapProject)
    {
        pomanageSapProject.setCreateTime(DateUtils.getNowDate());
        return pomanageSapProjectMapper.insertPomanageSapProject(pomanageSapProject);
    }

    /**
     * 修改SAP开发项目
     * 
     * @param pomanageSapProject SAP开发项目
     * @return 结果
     */
    @Override
    public int updatePomanageSapProject(PomanageSapProject pomanageSapProject)
    {
        pomanageSapProject.setUpdateTime(DateUtils.getNowDate());
        return pomanageSapProjectMapper.updatePomanageSapProject(pomanageSapProject);
    }

    /**
     * 批量删除SAP开发项目
     * 
     * @param projectIds 需要删除的SAP开发项目主键
     * @return 结果
     */
    @Override
    public int deletePomanageSapProjectByProjectIds(Long[] projectIds)
    {
        return pomanageSapProjectMapper.deletePomanageSapProjectByProjectIds(projectIds);
    }

    /**
     * 删除SAP开发项目信息
     * 
     * @param projectId SAP开发项目主键
     * @return 结果
     */
    @Override
    public int deletePomanageSapProjectByProjectId(Long projectId)
    {
        return pomanageSapProjectMapper.deletePomanageSapProjectByProjectId(projectId);
    }
}
