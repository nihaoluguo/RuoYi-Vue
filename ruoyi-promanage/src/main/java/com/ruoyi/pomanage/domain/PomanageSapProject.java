package com.ruoyi.pomanage.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * SAP开发项目对象 pomanage_sap_project
 * 
 * @author john
 * @date 2023-07-12
 */
public class PomanageSapProject extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 项目ID */
    private Long projectId;

    /** 项目编号 */
    @Excel(name = "项目编号")
    private String projectNo;

    /** 项目标题 */
    @Excel(name = "项目标题")
    private String projectTitle;

    /** 项目描述 */
    @Excel(name = "项目描述")
    private String projectDesc;

    public void setProjectId(Long projectId) 
    {
        this.projectId = projectId;
    }

    public Long getProjectId() 
    {
        return projectId;
    }
    public void setProjectNo(String projectNo) 
    {
        this.projectNo = projectNo;
    }

    public String getProjectNo() 
    {
        return projectNo;
    }
    public void setProjectTitle(String projectTitle) 
    {
        this.projectTitle = projectTitle;
    }

    public String getProjectTitle() 
    {
        return projectTitle;
    }
    public void setProjectDesc(String projectDesc) 
    {
        this.projectDesc = projectDesc;
    }

    public String getProjectDesc() 
    {
        return projectDesc;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("projectId", getProjectId())
            .append("projectNo", getProjectNo())
            .append("projectTitle", getProjectTitle())
            .append("projectDesc", getProjectDesc())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
