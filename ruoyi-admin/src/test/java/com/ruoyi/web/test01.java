package com.ruoyi.web;

import com.ruoyi.pill.domain.PillFactory;
import com.ruoyi.pill.service.IPillFactoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class test01 {
    @Autowired
    private IPillFactoryService pillFactoryService;

    @Test
    public void TestSelectFactory(){
        PillFactory factory=new PillFactory();
        factory.setFactoryName("工厂");
        System.out.println(pillFactoryService.selectPillFactoryList(factory));
    }

}
