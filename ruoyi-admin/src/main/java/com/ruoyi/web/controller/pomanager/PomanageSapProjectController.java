package com.ruoyi.web.controller.pomanager;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.pomanage.domain.PomanageSapProject;
import com.ruoyi.pomanage.service.IPomanageSapProjectService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * SAP开发项目Controller
 * 
 * @author ruoyi
 * @date 2023-07-12
 */
@RestController
@RequestMapping("/pomanage/sapproject")
public class PomanageSapProjectController extends BaseController
{
    @Autowired
    private IPomanageSapProjectService pomanageSapProjectService;

    /**
     * 查询SAP开发项目列表
     */
    @PreAuthorize("@ss.hasPermi('pomanage:sapproject:list')")
    @GetMapping("/list")
    public TableDataInfo list(PomanageSapProject pomanageSapProject)
    {
        startPage();
        List<PomanageSapProject> list = pomanageSapProjectService.selectPomanageSapProjectList(pomanageSapProject);
        return getDataTable(list);
    }

    /**
     * 导出SAP开发项目列表
     */
    @PreAuthorize("@ss.hasPermi('pomanage:sapproject:export')")
    @Log(title = "SAP开发项目", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, PomanageSapProject pomanageSapProject)
    {
        List<PomanageSapProject> list = pomanageSapProjectService.selectPomanageSapProjectList(pomanageSapProject);
        ExcelUtil<PomanageSapProject> util = new ExcelUtil<PomanageSapProject>(PomanageSapProject.class);
        util.exportExcel(response, list, "SAP开发项目数据");
    }

    /**
     * 获取SAP开发项目详细信息
     */
    @PreAuthorize("@ss.hasPermi('pomanage:sapproject:query')")
    @GetMapping(value = "/{projectId}")
    public AjaxResult getInfo(@PathVariable("projectId") Long projectId)
    {
        return success(pomanageSapProjectService.selectPomanageSapProjectByProjectId(projectId));
    }

    /**
     * 新增SAP开发项目
     */
    @PreAuthorize("@ss.hasPermi('pomanage:sapproject:add')")
    @Log(title = "SAP开发项目", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody PomanageSapProject pomanageSapProject)
    {
        pomanageSapProject.setCreateBy(getUsername());
        return toAjax(pomanageSapProjectService.insertPomanageSapProject(pomanageSapProject));
    }

    /**
     * 修改SAP开发项目
     */
    @PreAuthorize("@ss.hasPermi('pomanage:sapproject:edit')")
    @Log(title = "SAP开发项目", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody PomanageSapProject pomanageSapProject)
    {
        pomanageSapProject.setUpdateBy(getUsername());
        return toAjax(pomanageSapProjectService.updatePomanageSapProject(pomanageSapProject));
    }

    /**
     * 删除SAP开发项目
     */
    @PreAuthorize("@ss.hasPermi('pomanage:sapproject:remove')")
    @Log(title = "SAP开发项目", businessType = BusinessType.DELETE)
	@DeleteMapping("/{projectIds}")
    public AjaxResult remove(@PathVariable Long[] projectIds)
    {
        return toAjax(pomanageSapProjectService.deletePomanageSapProjectByProjectIds(projectIds));
    }
}
