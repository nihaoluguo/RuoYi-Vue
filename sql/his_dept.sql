/*
 Navicat Premium Data Transfer

 Source Server         : 本机
 Source Server Type    : MySQL
 Source Server Version : 50739
 Source Host           : localhost:3306
 Source Schema         : rouyi

 Target Server Type    : MySQL
 Target Server Version : 50739
 File Encoding         : 65001

 Date: 28/05/2023 00:54:35
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for his_dept
-- ----------------------------
DROP TABLE IF EXISTS `his_dept`;
CREATE TABLE `his_dept`  (
  `department_id` int(11) NOT NULL AUTO_INCREMENT,
  `department_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `department_code` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `registration_count` int(11) NULL DEFAULT 0,
  `department_leader` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `department_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `department_status` tinyint(1) NULL DEFAULT 1,
  `created_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
  `updated_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`department_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of his_dept
-- ----------------------------
INSERT INTO `his_dept` VALUES (1, '内科', 'NK', 100, '张医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '0021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (2, '外科', 'WK', 80, '李医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (3, '妇产科', 'FCK', 60, '王医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (4, '儿科', 'EK', 50, '赵医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (5, '眼科', 'YK', 30, '孙医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (6, '口腔科', 'KQK', 20, '钱医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (7, '皮肤科', 'PFK', 10, '周医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (8, '耳鼻喉科', 'EBHK', 5, '吴医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (9, '骨科', 'GK', 3, '郑医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');
INSERT INTO `his_dept` VALUES (10, '心理科', 'XLK', 1, '冯医生', '123456789', 1, 'admin', '2021-01-01 00:00:00', 'admin', '2021-01-01 00:00:00');

SET FOREIGN_KEY_CHECKS = 1;
