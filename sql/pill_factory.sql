CREATE TABLE pill_factory (
                              factory_id bigint(20) NOT NULL AUTO_INCREMENT,
                              factory_name varchar(50) NOT NULL,
                              factory_code varchar(50) NOT NULL,
                              contact varchar(30) NOT NULL,
                              phone varchar(30) NOT NULL,
                              keyword varchar(20) NOT NULL,
                              status char(1) DEFAULT NULL,
                              create_by varchar(50),
                              create_time datetime,
                              update_by varchar(50),
                              update_time datetime,
                              remark varchar(500),
                              PRIMARY KEY (factory_id) );

INSERT INTO pill_factory (factory_name, factory_code, contact, phone,
                          keyword, status, create_by, create_time,
                          update_by, update_time, remark)
VALUES ('ABC药厂', 'ABC001', '张三', '13888888888', '感冒', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产感冒药的药厂'),
       ('DEF药厂', 'DEF001', '李四', '13999999999', '头痛', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产头痛药的药厂'),
       ('GHI药厂', 'GHI001', '王五', '13666666666', '胃痛', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产胃痛药的药厂'),
       ('JKL药厂', 'JKL001', '赵六', '13555555555', '咳嗽', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产咳嗽药的药厂'),
       ('MNO药厂', 'MNO001', '钱七', '13777777777', '发烧', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产退烧药的药厂'),
       ('PQR药厂', 'PQR001', '孙八', '13111111111', '感冒', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产感冒药的药厂'),
       ('STU药厂', 'STU001', '周九', '13222222222', '头痛', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产头痛药的药厂'),
       ('VWX药厂', 'VWX001', '吴十', '13333333333', '胃痛', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产胃痛药的药厂'),
       ('YZA药厂', 'YZA001', '郑一', '13444444444', '咳嗽', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产咳嗽药的药厂'),
       ('BCD药厂', 'BCD001', '钱二', '13666666666', '发烧', '1', '管理员', NOW(), '管理员', NOW(), '这是一家专门生产退烧药的药厂');