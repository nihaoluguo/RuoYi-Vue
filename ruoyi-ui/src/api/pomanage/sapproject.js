import request from '@/utils/request'

// 查询SAP开发项目列表
export function listProject(query) {
  return request({
    url: '/pomanage/sapproject/list',
    method: 'get',
    params: query
  })
}

// 查询SAP开发项目详细
export function getProject(projectId) {
  return request({
    url: '/pomanage/sapproject/' + projectId,
    method: 'get'
  })
}

// 新增SAP开发项目
export function addProject(data) {
  return request({
    url: '/pomanage/sapproject',
    method: 'post',
    data: data
  })
}

// 修改SAP开发项目
export function updateProject(data) {
  return request({
    url: '/pomanage/sapproject',
    method: 'put',
    data: data
  })
}

// 删除SAP开发项目
export function delProject(projectId) {
  return request({
    url: '/pomanage/sapproject/' + projectId,
    method: 'delete'
  })
}
