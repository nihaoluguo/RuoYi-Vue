import request from '@/utils/request'

// 查询【请填写功能名称】列表
export function listFactory(query) {
  return request({
    url: '/pill/factory/list',
    method: 'get',
    params: query
  })
}

// 查询【请填写功能名称】详细
export function getFactory(factoryId) {
  return request({
    url: '/pill/factory/' + factoryId,
    method: 'get'
  })
}

// 新增【请填写功能名称】
export function addFactory(data) {
  return request({
    url: '/pill/factory',
    method: 'post',
    data: data
  })
}

// 修改【请填写功能名称】
export function updateFactory(data) {
  return request({
    url: '/pill/factory',
    method: 'put',
    data: data
  })
}

// 删除【请填写功能名称】
export function delFactory(factoryId) {
  return request({
    url: '/pill/factory/' + factoryId,
    method: 'delete'
  })
}
