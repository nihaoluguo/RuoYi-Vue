package com.ruoyi.pill.mapper;

import java.util.List;

import com.ruoyi.pill.domain.PillFactory;
import org.apache.ibatis.annotations.Param;

/**
 * 生产厂家Mapper接口
 * 
 * @author jhon
 * @date 2023-06-02
 */
public interface PillFactoryMapper 
{
    public int DeleteByid(Long id);
    /**
     * 查询生产厂家
     * 
     * @param factoryId 生产厂家主键
     * @return 生产厂家
     */
    public PillFactory selectPillFactoryByFactoryId(Long factoryId);

    /**
     * 查询生产厂家列表
     * 
     * @param pillFactory 生产厂家
     * @return 生产厂家集合
     */
    public List<PillFactory> selectPillFactoryList(PillFactory pillFactory);

    /**
     * 新增生产厂家
     * 
     * @param pillFactory 生产厂家
     * @return 结果
     */
    public int insertPillFactory(PillFactory pillFactory);

    /**
     * 修改生产厂家
     * 
     * @param pillFactory 生产厂家
     * @return 结果
     */
    public int updatePillFactory(PillFactory pillFactory);

    /**
     * 删除生产厂家
     * 
     * @param factoryId 生产厂家主键
     * @return 结果
     */
    public int deletePillFactoryByFactoryId(Long factoryId);

    /**
     * 批量删除生产厂家
     * 
     * @param factoryIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deletePillFactoryByFactoryIds(Long[] factoryIds);
}
